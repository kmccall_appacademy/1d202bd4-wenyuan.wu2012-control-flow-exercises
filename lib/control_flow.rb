# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    if ch == ch.downcase
      str.delete!(ch)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[str.length/2]
  else
    str[str.length/2 - 1..str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  number_of_vowels = 0
  VOWELS.each do |el|
    number_of_vowels += str.count(el)
    number_of_vowels += str.count(el.upcase)
  end
  number_of_vowels
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial = 1
  while num > 0
    factorial *= num
    num -= 1
  end
  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result_str = ""
  arr.each do |el|
    result_str << el.to_s
    result_str << separator
  end
  if separator != ""
    result_str[0..-2]
  else
    result_str
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  arr = str.split("")
  arr.each_with_index do |el,idx|
    if idx.odd?
      arr[idx] = el.upcase
    else
      arr[idx] = el.downcase
    end
  end
  arr.join("")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split(" ")
  arr.each_with_index do |el,idx|
    if el.length > 4
      arr[idx] = el.reverse
    end
  end
  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  arr.each_with_index do |el,idx|
    if el % 3 == 0 && el % 5 == 0
      arr[idx] = "fizzbuzz"
    elsif el % 3 == 0
      arr [idx] = "fizz"
    elsif el % 5 == 0
      arr[idx] = "buzz"
    else
      arr[idx] = el
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  arr.each do |el|
    arr = []
    arr << el
    reverse_arr = arr + reverse_arr
  end
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num > 1
    result = true
    divisor = num - 1
    while divisor > 1
      if num % divisor == 0
        result = false
        return result
      end
      divisor -= 1
    end
  result
  else
    return false
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  idx = num
  while idx > 0
    if num % idx == 0
      factors_arr << idx
    end
    idx -= 1
  end
  factors_arr.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors_arr = factors(num)
  factors_arr.each_with_index do |el, idx|
    if !prime?(el)
      factors_arr[idx] = nil
    end
  end
  factors_arr.compact
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  count_odds = 0
  arr.each do |el|
    if el.odd?
      count_odds += 1
    end
  end

  if count_odds == 1
    arr.each do |el|
      if el.odd?
        return el
      end
    end
  else
    arr.each do |el|
      if el.even?
        return el
      end
    end
  end
end
